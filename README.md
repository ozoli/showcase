# Welcome to the Amdatu Showcase #

### Background Information ###
This repository aims to showcase the benefits of using OSGi. Namely:

* True modularity, which is key to maintainable code
* An extremely fast code-save-test cycle in the IDE
* Incremental deployments (using Apache ACE)
* Service dynamics

[More info](http://paulonjava.blogspot.nl/2014/01/why-osgi-service-dynamics-are-useful.html)

### How do I get set up? ###

First we need to clone this repository using Git and then import the project into Eclipse. 

* git clone git@bitbucket.org:amdatu/showcase.git
* Open [Eclipse](https://eclipse.org/)
* Make sure the [Amdatu Plugins](https://marketplace.eclipse.org/content/amdatu-plugins) are installed. Use Help -> Eclipse MarketPlace to check.
* Make sure [BndTools](http://bndtools.org/installation.html#marketplace) is installed. Again use Help -> Eclipse MarketPlace to check.
* Select File -> Switch Workspace -> Other
* Select the directory you cloned the showcase to, click Ok
* Import the projects, File -> Import, General -> Existing Projects into Workspace.

To run the showcase do the following:

* Make sure you have [MongoDB](https://docs.mongodb.org/manual/installation/) installed and running on the default port (27017). 
* In the showcase workspace in Eclipse, open the run package, right click on webshop.bndrun select Run As -> Bnd OSGi Run Launcher
* Open [http://localhost:8080](http://localhost:8080) to see the webshop

### Contribution guidelines ###

* Please feel free to fork and submit pull requests.

### Who do I talk to? ###

* Ask a question on [http://stackoverflow.com/questions/tagged/amdatu](Stackoverflow) using the Amdatu tag
* [The Amdatu Mailing Lists](http://amdatu.org/mailinglist.html)